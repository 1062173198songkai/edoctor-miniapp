// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    sourceType: ['camera', 'album'],
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    paco2: '',
    pao2: '',
    ph: '',
    ab: '',
    sb: '',
    be: '',
    hco3: '',
    cl: '',
    k: '',
    na: '',
    ca: '',
    photosrc:undefined
  },
  Pao2(e) {
    this.setData({
      pao2: e.detail.value
    })
  },
  Paco2(e) {
    this.setData({
      paco2: e.detail.value
    })
  },
  AB(e) {
    this.setData({
      ab: e.detail.value
    })
  },
  SB(e) {
    this.setData({
      sb: e.detail.value
    })
  },
  PH(e) {
    this.setData({
      ph: e.detail.value
    })
  },
  BE(e) {
    this.setData({
      be: e.detail.value
    })
  },
  Hco3(e) {
    this.setData({
      hco3: e.detail.value
    })
  },
  CL(e) {
    this.setData({
      cl: e.detail.value
    })
  },
  NA(e) {
    this.setData({
      na: e.detail.value
    })
  },
  K(e) {
    this.setData({
      k: e.detail.value
    })
  },
  CA(e) {
    this.setData({
      ca: e.detail.value
    })
  },
  takePhoto(){
    const that = this;
    wx.showActionSheet({
      itemList: ['拍照','相册'],
      itemColor:'',
      success:function(res){
        if(!res.cancel){
          that.chooseImage(res.tapIndex)
        }
      },
      fail:function(res){
        console.log('调用失败')
      },
      complete:function(res){}
    })
  },
  chooseImage(tapIndex){
    const checkeddata = true;
    const that = this;
    wx.chooseImage({
      count: 1,
      sizeType:['original','compressed'],
      sourceType:[that.data.sourceType[tapIndex]],
      success(res){
        const temFilePaths = res.tempFilePaths
        wx.setStorage('tempFilePaths',temFilePaths)
        console.log('tempFilePaths',temFilePaths)
        console.log('tempFilePaths',temFilePaths[0])
        wx.showLoading({
          title: '识别中',
        });
        wx.uploadFile({
          filePath: temFilePaths[0],
          name: 'file',
          formData: {
            description: that.data.userInfo.nickName
          }
          ,url: 'http://localhost:19020/upload',
          success(res){
            console.log(res);
            console.log('res.data',res.data);
            const resultData = JSON.parse(res.data);
            console.log('resultData',resultData.result);
            that.process_ocr_result(resultData);
          },fail(res){
            console.log(res);
            that.ocr_alert_message();
          },complete(){
            wx.hideLoading({
              success: (res) => {},
              fail:(res) =>{
                wx.navigateTo({
                  url: '/pages/index/index',
                })
              }
            })
          }
        })
      }
    })
  },
  ocr_alert_message:function(){
    wx.showToast({
      icon:"none",
      title: '为检测出化验单,如遇到识别失败,可去除敏感信息后，通过小程序右上角反馈给我们图片，我们将进行适配。',
      duration:5000
    })
  },
  process_ocr_result:function(result){
    result = result.result;
    console.log('result',result);
    let K = result["K+"];
    let PaO2 = result["PaO2"];
    let PaCo2 = result["PaCO2"];
    let AB = result["AB"];
    let PH = result["PH"];
    let HCO3 = result["HCO3-"];
    let Ca = result["Ca2+"];
    let Na = result["Na+"];
    let SB = result["SB"];
    let BE = result["BE"];
    let Cl = result["Cl-"];
    console.log('K',K,typeof(K));
    let length = 0;
    if(K != null && K != undefined){
      this.setData({
        k:K
      });
      length ++;
    }
    if(PaO2 != null &&  PaO2!= undefined){
      this.setData({
        pao2:PaO2
      })
      length ++;
    }
    if(AB != null && AB != undefined){
      this.setData({
        ab:AB
      })
      length ++;
    }
    if(PaCo2 != null && PaCo2!= undefined){
      this.setData({
        paco2:PaCo2
      })
      length ++;
    }
    if(PH != null && PH != undefined){
      this.setData({
        ph:PH
      })
      length ++;
    }
    if(HCO3 != null && HCO3 != undefined){
      this.setData({
        hco3:HCO3
      })
      length ++;
    }
    if(Ca != null && Ca != undefined){
      this.setData({
        ca:Ca
      })
      length ++;
    }
    if(Na != null && Na != undefined){
      this.setData({
        na:Na
      })
      length ++;
    }
    if(SB != null && SB != undefined){
      this.setData({
        sb:SB
      })
      length ++;
    }
    if(BE != null && BE != undefined){
      this.setData({
        be:BE
      })
      length ++;
    }
    if(Cl != null && Cl != undefined){
      this.setData({
        cl:Cl
      })
      length ++;
    }
    if(length <= 9){
      this.ocr_alert_message();
    }
  },
  check_breath: function (e) {
    if (this.data.pao2 < 60 && this.data.paco2 >= 35 && this.data.paco2 <= 45) app.globalData.breath = "I型呼吸衰竭";
    else if (this.data.pao2 < 60 && this.data.paco2 > 50) app.globalData.breath = "II型式呼吸衰竭";
    else app.globalData.breath = "无呼吸衰竭情况";
    //酸碱失衡情况
    //酸中毒
    if (this.data.ph < 7.35) {
      if (this.data.paco2 > 45) {
        if (this.data.be <= 3 && this.data.be >= -3) app.globalData.acid_base = "呼酸";
        else if (this.data.be > 3) app.globalData.acid_base = "呼酸肾代偿";
        else app.globalData.acid_base = "呼酸合并代酸";
      } else if (this.data.paco2 >= 35 && this.data.paco2 <= 45) {
        if (this.data.be < -3) {
          app.globalData.acid_base = "代酸";
        } else app.globalData.acid_base = "数据可能有误,考虑代酸";
      } else {
        if (this.data.be < -3) app.globalData.acid_base = "代酸呼吸代偿";
        else app.globalData.acid_base = "数据可能有误,考虑代酸呼吸代偿";
      }
    }

    //碱中毒
    else if (this.data.ph > 7.45) {
      if (this.data.paco2 < 35) {
        if (this.data.be <= 3 && this.data.be >= -3) app.globalData.acid_base = "呼碱";
        else if (this.data.be < -3) app.globalData.acid_base = "呼碱肾代偿";
        else app.globalData.acid_base = "呼碱合并代碱";
      } else if (this.data.paco2 >= 35 && this.data.paco2 <= 45) {
        if (this.data.be > 3) app.globalData.acid_base = "代碱";
        else app.globalData.acid_base = "数据可能有误,考虑代碱";
      } else {
        if (this.data.be > 3) app.globalData.acid_base = "代碱呼吸代偿";
        else app.globalData.acid_base = "考虑代碱呼吸代偿";
      }
    }

    //ph正常
    else {
      if (this.data.paco2 > 45) {
        if (this.data.be > 3) app.globalData.acid_base = "呼酸合并代碱";
        else app.globalData.acid_base = "数据可能有误,考虑呼酸合并代碱";
      } else if (this.data.paco2 < 35) {
        if (this.data.be < -3) app.globalData.acid_base = "呼碱合并代酸";
        else app.globalData.acid_base = "数据可能有误,考虑呼碱合并代酸";
      } else {
        if (this.data.be <= 3 && this.data.be >= -3) app.globalData.acid_base = "正常";
        else app.globalData.acid_base = "数据可能有误,考虑正常";
      }
    }
  },
  check_ox: function (e) {
    if (this.data.pao2 < 45) app.globalData.ox = "严重缺氧";
    else if (this.data.pao2 <= 60 && this.data.pao2 >= 45) app.globalData.ox = "中度缺氧";
    else if (this.data.pao2 <= 80 && this.data.pao2 >= 60) app.globalData.ox = "轻度缺氧";
    else app.globalData.ox = "无明显缺氧状况";
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
      console.log('userInfo',this.data.userInfo);
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
        console.log('userInfo',this.data.userInfo);
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  reset(){
    this.setData({
      paco2: '',
      pao2: '',
      ph: '',
      ab: '',
      sb: '',
      be: '',
      hco3: '',
      cl: '',
      k: '',
      na: '',
      ca: ''
    })
  },
  getResult(){
   this.check_ox();
   this.check_breath();
   app.globalData.K = this.data.k;
   app.globalData.Na = this.data.na;
   app.globalData.Ca = this.data.ca;
   wx.navigateTo({
     url: '/pages/result/result',
   })
  },
  getUserInfo(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})